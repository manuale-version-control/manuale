# Version Control - Git

1. [Cos'è Git?](#cosè-git)
2. [Comandi principali di git](#comandi-principali-di-git)
3. [I Branch](#i-branch)
4. [Repository remota](#repository-remota)

---

<br>

## Cos'è Git?

Git è un sistema di controllo versione distribuito, ovvero uno strumento che consente di tenere traccia delle modifiche apportate a un progetto software nel tempo, consentendo agli sviluppatori di collaborare in modo efficiente e gestire le versioni del codice.

<br>

---

<br>

## Comandi principali di Git

Qui sono elencati i comandi principali di Git da utilizzare tramite il terminale `git bash`.



<br>

### <b>Inizializzazione di una repository</b>

Per creare una nuova repository Git, è necessario utilizzare il comando `git init` nella cartella in cui si vuole creare la repository.

```bash
git init
```

<br>

---

<br>

### <b>Aggiunta dei file alla repository</b>

Per aggiungere i file alla repository, è necessario utilizzare il comando `git add` "nome_file" per aggiungere il file specificato alla staging area.

```bash
git add <nome_file>
```

<br>

---

<br>

### <b>Creazione di un commit</b>

Una volta che i file sono stati aggiunti alla staging area, è necessario creare un commit per salvare le modifiche nella repository. Utilizzare il comando `git commit -m "descrizione del commit"` per creare un commit.

```bash
git commit -m "descrizione del commit"
```

<br>

---

<br>

### <b>Visualizzazione dello stato della repository</b>

Per visualizzare lo stato attuale della repository, è possibile utilizzare il comando `git status`.

```bash
git status
```

<br>

---

<br>

### <b>Visualizzazione della cronologia dei commit</b>

Per visualizzare la cronologia dei commit, è possibile utilizzare il comando `git log`.

```bash
git log
```

<br>

---

<br>

## <b>I Branch</b>

<br>

### <b>Creazione di un branch</b>

Per creare un nuovo branch, utilizzare il comando `git branch <nome_branch>`.

```bash
git branch <nome_branch>
```

<br>

---

<br>

### <b>Spostarsi su un branch</b>

Per spostarsi su un branch esistente, utilizzare il comando git checkout `<nome_branch>`.

```bash
git checkout <nome_branch>
```

<br>

---

<br>

### <b>Unione dei branch</b>

Per unire le modifiche apportate in un altro branch al branch corrente, utilizzare il comando git merge `<nome_branch>`.

```bash
git merge <nome_branch>
```

<br>

---

<br>

### <b>Clonazione di una repository</b>

Per clonare una repository esistente, è possibile utilizzare il comando `git clone <url_repository>`.

```bash
git clone <url_repository>
```

<br>

---

<br>

## <b>Repository Remota</b>

<br>

### <b>Invio delle modifiche alla repository remota</b>
Per inviare le modifiche apportate alla repository remota, utilizzare il comando `git push`.

```bash
git push
```

<br>

---

<br>

### <b>Aggiornamento della repository locale con le modifiche della repository remota</b>

Per aggiornare la repository locale con le modifiche apportate alla repository remota, utilizzare il comando `git pull`.

```bash
git pull
```